module.exports =[
  {
    "data":{
      "weather_monitoring":[
          {
            "tornado":"0",
            "timestamp":"22/10/2017-06:00:00",
            "rain":"0",
            "month":"10",
            "winddir":"WNW",
            "pressure":"1017",
            "windspeed":"9.3",
            "hail":"0",
            "year":"2017",
            "timezone":"America/Sao_Paulo",
            "day":"22",
            "icon":"clear",
            "temperature":"22.0",
            "hour":"06",
            "min":"00",
            "thunder":"0",
            "snow":"0",
            "humidity":"60",
            "fog":"0",
            "location":{
                "lat":-23.563885109053672,
                "lon":-46.652401318975826
            },
            "conditions":"Clear"
          }
      ]
    }
  },

  
]