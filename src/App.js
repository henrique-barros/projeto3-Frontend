/* global Plotly:true */
/* global google */

import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import createPlotlyComponent from 'react-plotly.js/factory'
import { GoogleMap, Marker,
  withScriptjs,
  withGoogleMap,
  Polyline,
  KmlLayer } from "react-google-maps"

import { DatePicker, TimePicker, RaisedButton, Checkbox, SelectField, MenuItem, Slider } from 'material-ui'
import Anomalies from './anomalies'
import { googleApiKey } from './keys'
var moment = require('moment');


const Plot = createPlotlyComponent(Plotly);

const dates = {
  1: '2017-10-22',
  2: '2017-10-23',
  3: '2017-10-24',
  4: '2017-10-25',
  5: '2017-10-26',
  6: '2017-10-27',
  7: '2017-10-28',
}

class App extends Component {
  constructor(props) {
    super(props)
    const events = Anomalies.map((val) => ({
      lat: val.Latitute,
      lng: val.Longitude,
      hour: val.Hour.slice(0,2),
      date: moment(val.Date, 'DD-MM-YYYY').format('YYYY-MM-DD'),
      event: `${val.Event} -  ${val.Event_definition}`
    }))
    this.state = {
      events,
      weather: [],
      showRegionLimits: false,
      date: 1,
      time: moment({ hour:0, minute:0 }),
      tempTime: moment({ hour:0, minute:0 }),
      showOnlyAnomalies: true,
      timers: []
    }
    this.changeDate = this.changeDate.bind(this)
    this.changeTime = this.changeTime.bind(this)
    this.changeShowRegionLimits = this.changeShowRegionLimits.bind(this)
    this.startTimer = this.startTimer.bind(this)
    this.stopTimer = this.stopTimer.bind(this)
    this.releaseTimePicker = this.releaseTimePicker.bind(this)
    this.changeShowOnlyAnomalies = this.changeShowOnlyAnomalies.bind(this)
    this.changeTimeWithInterval = this.changeTimeWithInterval.bind(this)
  }
  
  componentWillMount() {
    //fetch('http://192.168.0.106:8000/data?collection=anomalies')
    fetch('http://localhost:8000/data?collection=anomalies')
      .then((response) => {
        return response.json()
      })
      .then((responseJson) => {
        this.setState({data: responseJson})
      })
      .catch((err) => {
      })

    //fetch('http://192.168.0.106:8000/data?collection=real_anomalies')
    fetch('http://localhost:8000/data?collection=real_anomalies')
      .then((response) => {
        return response.json()
      })
      .then((responseJson) => {
        this.setState({realAnomalies: responseJson})
      })
      .catch((err) => {
      })

    //fetch('http://192.168.0.106:8000/data?collection=sensor_values')
    fetch('http://localhost:8000/data?collection=sensor_values')
      .then((response) => {
        return response.json()
      })
      .then((responseJson) => {
        this.setState({weather: responseJson.map((x) =>
          ({
            ...x,
            date: x.year + '-'
              + x.month + '-'
              + x.day,
            lat: x.location.lat,
            lng: x.location.lon
          })
        )})
      })
      .catch((err) => {
      })
  }

  changeDate(event, index, val) {
    this.setState({
      date: val
    })
    //fetch('http://192.168.0.106:8000/data?collection=anomalies&date=' + dates[val])
    fetch('http://localhost:8000/data?collection=anomalies&date=' + dates[val])
      .then((response) => {
        return response.json()
      })
      .then((responseJson) => {
        this.setState({data: responseJson})
      })
      .catch((err) => {
      })
  }

  //Não utilizar timer, mas sim um slider!
  startTimer() {
    var timer = setInterval(() => {
      this.changeTimeWithInterval();
    }, 1000);
    

    this.setState({
      timers: [...this.state.timers, timer]
    })
  }

  stopTimer() {
    const timers = this.state.timers
    timers.forEach((t) => {
      clearInterval(t);
    })
    this.setState({ timers: [] })
  }

  // changeTime(event, time) {
  //   this.setState({
  //     time: time
  //   })
  // }

  changeTimeWithInterval() {
    console.log('mudou tempo')
    const { time } = this.state
    if (time.format('HH') !== 24) {
      this.setState({
        time: time.add('hour', 1)
      })
    }
    else {
      this.setState({
        time: moment({ hour:0, minute:0 })
      })
      this.stopTimer();
    }
    console.log(this.state.time.format('HH'))
  }

  changeTime(e, val) {
    // if (time.format('HH') !== 24) {
    //   this.setState({
    //     time: time.add('hour', 1)
    //   })
    // }
    // else {
    //   this.setState({
    //     time: moment({ hour:00, minute:00 })
    //   })
    //   this.stopTimer();
    // }
    this.setState({
      time: moment({hour: val * 24})
    })
  }

  releaseTimePicker() {
    this.setState({
      time: this.state.tempTime
    })
  }

  changeShowRegionLimits(ev, val) {
    this.setState({
      showRegionLimits: val
    })
  }

  changeShowOnlyAnomalies(ev, val) {
    this.setState({
      showOnlyAnomalies: val
    })
  }

  render() {
    let { data, date, time, events, weather, showRegionLimits, tempTime, showOnlyAnomalies, realAnomalies } = this.state

    const     googleMapURL =
    "https://maps.googleapis.com/maps/api/js?key="+ googleApiKey  +"&v=3.exp&libraries=geometry,drawing,places"

    console.log('dados')
    console.log(data)

    console.log(realAnomalies)

    if (data && realAnomalies) {
      if (showOnlyAnomalies) {
        // data = realAnomalies
      } 
      else {
        console.log('somando os dois')
        console.log(data)
        console.log(realAnomalies)
        data = data.concat(realAnomalies)
      }
    }
    else {
      data = []
    }

    console.log('anomalias')
    console.log(data)

    if (date !== undefined) {
      let formattedDate = dates[date]
      console.log(formattedDate)
      if (data) {
        console.log('filtro data')
        console.log(data)
        data = data.filter((x) => x.date === formattedDate)
      }
      if (events)
        events = events.filter((x) => x.date === formattedDate)
      if (weather)
        weather = weather.filter((x) => x.date === formattedDate)
    }

    console.log('anomalias filtradas por data')
    
    console.log(data)

    if (time) {
      var formattedTime = moment(time).format('HH')
      if (data)
        data = data.filter((x) => x.hour === parseInt(formattedTime))
      events = events.filter((x) => x.hour === formattedTime)
      weather = weather.filter((x) => x.hour === parseInt(formattedTime))
    }

    console.log('anomalias filtradas por tempo')
    console.log(data)


    return (
      data ?
        <div className="App-container">
          <div style={{ height: `90vh`, width: '80%' }}>
            <MyMapComponent
              isMarkerShown
              googleMapURL={googleMapURL}
              loadingElement={<div style={{ height: `100%` }} />}
              containerElement={<div style={{ height: `100%`, width: '100%' }} />}
              mapElement={<div style={{ height: `100%` }} />}
              anomalies={data}
              events={events}
              weather={weather}
              showRegionLimits={showRegionLimits}
            />
            <div style={{height: '10vh', marginTop: 'auto', display: 'flex'}}>
              <div style={{marginTop: 'auto'}}>Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
            </div>
          </div>
          <SideComponent releaseTimePicker={this.releaseTimePicker} tempTime={tempTime} date={date} time={time} changeDate={this.changeDate} changeTime={this.changeTime} showRegionLimits={showRegionLimits} changeShowRegionLimits={this.changeShowRegionLimits}
            changeShowOnlyAnomalies={this.changeShowOnlyAnomalies} showOnlyAnomalies={showOnlyAnomalies}
            startTimer={this.startTimer} stopTimer={this.stopTimer} />
        </div>
      :
        <div>Carregando...</div>
    );
  }
}

const SideComponent = (props) => 
  <div className="Side-container">
    <div className="Scale" />
    <div className="Scale-text-container" style={{width: '95%'}}>
      <div>fast</div>
      <div>slow</div>
    </div>
    <div className="Date-picker" style={{maxWidth: '95%'}}>
      {/* <DatePicker minDate={new Date('2017-10-23')} maxDate={new Date('2017-10-29')} value={props.date} onChange={props.changeDate} /> */}
      <SelectField
          style={{width: '100%'}}
          floatingLabelText="Date"
          value={props.date}
          onChange={props.changeDate}
        >
          <MenuItem value={1} key={1} primaryText="22/10/2017" />
          <MenuItem value={2} key={2}  primaryText="23/10/2017" />
          <MenuItem value={3} key={3} primaryText="24/10/2017" />
          <MenuItem value={4} key={4} primaryText="25/10/2017" />
          <MenuItem value={5} key={5} primaryText="26/10/2017" />
          <MenuItem value={6} key={6} primaryText="27/10/2017" />
          <MenuItem value={7} key={7} primaryText="28/10/2017" />
        </SelectField>
    </div>
    <div style={{marginTop: '16px', maxWidth: '95%'}}>
      Hour:
    </div>
    
    <div style={{marginTop: '16px', maxWidth: '95%'}}>
      <Slider value={parseInt(moment(props.time).format('HH'))/24} onChange={props.changeTime} />
    </div>

    {/* <div style={{marginTop: '16px', maxWidth: '90%'}}>
      Temp Hora: {props.tempTime.format('HH') + ':00'}
    </div> */}

    <div style={{marginTop: '16px', maxWidth: '95%'}}>
      Hora: {props.time.format('HH') + ':00'}
    </div>
    {/* <div className="Time-picker">
      <TimePicker minutesStep={60} value={props.time} onChange={props.changeTime} />
      <RaisedButton onClick={() =>props.changeTime(null,null)}>Clear time</RaisedButton>
    </div> */}
    <div style={{marginTop: '16px', maxWidth: '95%'}}>
      <Checkbox
        label="Show region limits"
        checked={props.showRegionLimits}
        onCheck={props.changeShowRegionLimits}
      />
    </div>
    <div style={{marginTop: '16px', maxWidth: '95%'}}>
      <Checkbox
        label="Show only anomalies"
        checked={props.showOnlyAnomalies}
        onCheck={props.changeShowOnlyAnomalies}
      />
    </div>
    <div style={{marginTop: '16px'}}>
      <RaisedButton onClick={() => props.startTimer()}>Start timer</RaisedButton>
    </div>
    <div style={{marginTop: '16px'}}>
      <RaisedButton onClick={() => props.stopTimer()}>Stop timer</RaisedButton>
    </div>

    
  </div>

const getColor = (value) => {
  //value from 0 to 1
  var hue=((value)*120).toString(10);
  return ["hsl(",hue,",100%,50%)"].join("");
}

const iconFlood = {
  url: 'https://www.dropbox.com/s/abl3yricqmlgc6b/flood.png?dl=1',
  scaledSize: new google.maps.Size(24, 24)
}

const iconSoccer = {
  url: 'https://www.dropbox.com/s/hwdsqxo93f9fact/soccer-ball-variant.png?dl=1',
  scaledSize: new google.maps.Size(24, 24)
}

const iconVolleyball = {
  url: 'https://www.dropbox.com/s/j2k4lj8wtxiyl8u/volleyball.png?dl=1',
  scaledSize: new google.maps.Size(24, 24)
}

const iconConcert = {
  url: 'https://www.dropbox.com/s/8wnecqk37cqe267/jazz-saxophone.png?dl=1',
  scaledSize: new google.maps.Size(24, 24)
}

const getEventMarker = (event) => {
  let icon = null
  if (event.event.split(' ')[0] === 'Alagamento')
    icon = iconFlood
  else if (event.event.split(' ')[0] === 'Futebol')
    icon = iconSoccer
  else if (event.event.split(' ')[0] === 'Voleiball')
    icon = iconVolleyball
  else if (event.event.split(' ')[0] === 'Show')
    icon = iconConcert


  if (icon) {
    return <Marker
              position={event}
              title={`${event.event}\n${event.date} ${event.hour}:00`}
              labelVisible={false}
              icon={icon}
            ></Marker>
  }
  return <Marker
          position={event}
          title={`${event.event}\n${event.date} ${event.hour}:00`}
          labelVisible={false}
        ></Marker>
}

const MyMapComponent = withGoogleMap((props) =>
    <GoogleMap
      defaultZoom={12}
      defaultCenter={{ lat: -23.547830, lng: -46.649096 }}
    >
      {
        props.showRegionLimits ?
          <KmlLayer options={{ preserveViewport: true }} url="https://www.google.com.br/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=0ahUKEwj3o4_HqIDZAhUIG5AKHTMPBXEQFggoMAA&url=http%3A%2F%2Fwww.prefeitura.sp.gov.br%2Fcidade%2Fsecretarias%2Fupload%2Fsaude%2Farquivos%2Finfambiental%2FDistritosAdministrativos.kmz&usg=AOvVaw2ywKaWRmbRlX_bwdEtquL3"></KmlLayer>
          : null
      }
      {
        props.anomalies.map((anomaly) => {
          console.log(anomaly)
           return <Polyline
            path={anomaly.locations}
            options={{strokeColor: getColor(anomaly.avg_speed/30)}}
          ></Polyline>
        }
        )
      }
      {
        props.events.map((event) => 
          getEventMarker(event)
        )
      }
      {
        props.weather.map((weather) => 
          weather.rain === 1 ?
          <Marker
            position={weather}
            title={`chuva\n${weather.date}`}
            icon={{
              url: 'https://www.dropbox.com/s/0da3weznftf8bxa/storm.png?dl=1',
              scaledSize: new google.maps.Size(24, 24)
            }}
          ></Marker> : null
        )
      }
      
      {props.isMarkerShown && <Marker position={{ lat: -34.397, lng: 150.644 }} />}
    </GoogleMap>
  
)

export default App;
